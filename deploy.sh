#!/bin/bash

horbar_addr=$1
horbar_repo=$2
project=$3
version=$4
container_port=$5
host_port=$6




imageName=$horbar_addr/$horbar_repo/$project:$version

[ $# -eq 6 ] || {
  echo "Usage: $0 horbar_addr horbar_repo project version container_port host_port"
  exit 2

}

containerId=`docker ps  -aqf name=$project`

if [ "$containerId" != "" ]; then
  docker stop $containerId
  docker rm $containerId
fi

imageId=`docker images -qf reference="$imageName"`

if [ "$imageId" != "" ]; then
  docker rmi -f $imageId
fi

docker login -u admin -p Harbor12345 $horbar_addr
docker pull $imageName
docker run -d -p $host_port:$container_port --name $project $imageName

echo "Success"
